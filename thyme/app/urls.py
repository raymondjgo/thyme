from django.conf.urls import patterns, url

from app import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^inglist', 'app.views.ingredient_full_list'),
	url(r'^ownedpantry', 'app.views.user_list'),
	url(r'^additem', 'app.views.add_item'),
	url(r'^deleteitem', 'app.views.delete_item'),
	url(r'^updateitem', 'app.views.update_item'),
	url(r'^findfulluser', 'app.views.find_recipes_full_user'),
	url(r'^findpartialuser', 'app.views.find_recipes_partial_user'),
	url(r'^findpartialmultiuser', 'app.views.find_recipes_partial_multiple_user'),
	url(r'^findfullmultiuser', 'app.views.find_recipes_full_multiple_user'),
	url(r'^findpartial', 'app.views.find_recipes_partial'),
	url(r'^findfull', 'app.views.find_recipes_full'),
	url(r'^search', 'app.views.search'),
	url(r'^findfriendforrecipe', 'app.views.find_friend'),
	url(r'^findfriends', 'app.views.find_friends'),
	url(r'^friendrequest', 'app.views.friend_request'),
	url(r'^addfriendship', 'app.views.add_friendship'),
	url(r'^createuser', 'app.views.create_user'),
	url(r'^authenticate', 'app.views.authenticate_user'),
	url(r'^addrecipe', 'app.views.add_recipe'),
	url(r'^recipelist', 'app.views.recipe_full_list'),
	url(r'^addtorecipe', 'app.views.add_ingredient_to_recipe'),
	url(r'^emptylist', 'app.views.user_empty_list'),
	url(r'^ingredientsfromrecipe', 'app.views.recipe_ingredient_list'),
	url(r'^random', 'app.views.random'),
)

