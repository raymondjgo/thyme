from django.db import models
from django.contrib.auth.models import User

class Ingredient(models.Model):
	unit = models.CharField(default = 'none', max_length = 20)
	name = models.CharField(max_length = 100)

class OwnedPantry(models.Model):
	item = models.ForeignKey(Ingredient)
	owner = models.ForeignKey(User)
	amount = models.IntegerField()
	is_empty = models.BooleanField(default=False)

class Recipe(models.Model):
	name = models.CharField(max_length = 140)
	directions = models.CharField(max_length = 3000)
	rating = models.IntegerField()
	reviews = models.CharField(max_length = 20000)
	category = models.CharField(max_length = 25)
	time = models.IntegerField() #minutes
	uses_stove = models.BooleanField(default=False)
	uses_microwave = models.BooleanField(default=False)
	uses_oven = models.BooleanField(default=False)
	is_kosher = models.BooleanField(default=False)
	is_vegetarian = models.BooleanField(default=False)
	is_nutfree = models.BooleanField(default=False)
	is_glutenfree = models.BooleanField(default=False)
	is_lactosefree = models.BooleanField(default=False)

class RecipePantry(models.Model):
	item = models.ForeignKey(Ingredient)
	owner = models.ForeignKey(Recipe)
	amount = models.IntegerField()

class Friendship(models.Model):
	to_user = models.ForeignKey(User, related_name='friendship_to_user')
	from_user = models.ForeignKey(User, related_name='friendship_from_user')
