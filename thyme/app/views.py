from django.http import HttpResponse
from django.shortcuts import render
from app.models import *
from django.core import serializers
from django.db.models import Q
from django.utils import simplejson
from django.core.mail import send_mail
from django.db import IntegrityError
from django.contrib.auth import authenticate
import random

def index(request):
	return render(request,'index.html')

def ingredient_full_list(request):
	ingredient_list = Ingredient.objects.order_by('-name')

	json = simplejson.dumps( {"ingredients" : [{'name': i.name, 'unit': i.unit} for i in ingredient_list]} )

	return HttpResponse(json)

def user_list(request):
	uname = request.GET.get('username')
	pantry = OwnedPantry.objects.filter(owner__username=uname).select_related()

	json = simplejson.dumps( {"ingredients" :[{'item': p.item.name, 'unit': p.item.unit, 'amount': p.amount, 'is_empty': p.is_empty} for p in pantry]} )

	return HttpResponse(json)

def add_item(request):
	uname = request.GET.get('username')
	ing_name = request.GET.get('name')
	empty = int(request.GET.get('is_empty'))
	amt = int(request.GET.get('amount'))	

	try:
		ingredient = Ingredient.objects.get(name=ing_name)
	except Ingredient.DoesNotExist:
		return HttpResponse(status=400)

	try:
		user = User.objects.get(username=uname)
	except User.DoesNotExist:
		return HttpResponse(status=400)

	entry = OwnedPantry.objects.filter(item_id=ingredient.id,owner_id=user.id)
	if entry.count() > 0:
		return HttpResponse(status=400)

	to_add = OwnedPantry(item=ingredient,owner=user,is_empty=empty,amount=amt)
	to_add.save()

	return HttpResponse(status=200)
	
def delete_item(request):
	uname = request.GET.get('username')
	ing_name = request.GET.get('name')	

	try:
		ingredient = Ingredient.objects.get(name=ing_name)
	except Ingredient.DoesNotExist:
		return HttpResponse(status=400)

	try:
		user = User.objects.get(username=uname)
	except User.DoesNotExist:
		return HttpResponse(status=400)

	entry = OwnedPantry.objects.filter(item_id=ingredient.id,owner_id=user.id)
	if entry.count() == 0:
		return HttpResponse(status=400)

	for e in entry:
		e.delete()

	return HttpResponse(status=200)

def update_item(request):
	uname = request.GET.get('username')
	ing_name = request.GET.get('name')
	empty = int(request.GET.get('is_empty'))
	amt = int(request.GET.get('amount'))

	try:
		ingredient = Ingredient.objects.get(name=ing_name)
	except Ingredient.DoesNotExist:
		return HttpResponse(status=400)

	try:
		user = User.objects.get(username=uname)
	except User.DoesNotExist:
		return HttpResponse(status=400)

	entry = OwnedPantry.objects.filter(item_id=ingredient.id,owner_id=user.id)
	if entry.count() == 0:
		return HttpResponse(status=400)

	for e in entry:
		e.amount = amt
		e.is_empty = empty
		e.save()

	return HttpResponse(status=200)

def form_json(recipes):
	rec_ings="["
	for r in recipes:
		str = '%s, "ingredients": [' % serializers.serialize('json',[r,])[43:-3]
		rec_ings = rec_ings + str
		rps = RecipePantry.objects.filter(owner_id = r.id)
		for rp in rps:
			ing = Ingredient.objects.get(pk = rp.item_id)
			str = '{"name": "%s","unit": "%s","amount": "%s"' % (ing.name, ing.unit, rp.amount)
			str = str + "},"
			rec_ings = rec_ings + str
		rec_ings = rec_ings[:-1] + "]},"
	rec_ings = rec_ings[:-1] + "]" 

	return rec_ings


def find_recipes_partial(request):
	params = request.GET.get('params')
	items = params.split(',')	

	ingredients = Ingredient.objects.filter(name__in=items).all()

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "SELECT r.* FROM app_recipe r JOIN app_recipepantry rp ON rp.owner_id = r.id AND rp.item_id IN (%d" % ingredients[0].id

	for i in ingredients[1:]:
		temp = ",%d" % i.id
		str = str + temp
	
	str = str + ') GROUP BY r.name;'

	recipes = Recipe.objects.raw(str)

        if len(list(recipes)) == 0:
                return HttpResponse(' { "recipes" :  [] } ')

	rec_ings = '{ "recipes" : ' + form_json(recipes) + '}'
		
	return HttpResponse(rec_ings)



def find_recipes_full(request):
	params = request.GET.get('params')
	items = params.split(',')
	
	ingredients = Ingredient.objects.filter(name__in=items).all()

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "select * from app_recipe where id in (select rp.owner_id from app_recipepantry as rp left join (select %d as id" % ingredients[0].id

	for i in ingredients[1:]:
		temp = " union all select %d" % i.id
		str = str + temp

	temp = ') search on rp.item_id = search.id group by rp.owner_id having count(case when search.id is null then 1 end) = 0);'
	
	str = str + temp

	recipes = Recipe.objects.raw(str)

        if len(list(recipes)) == 0:
                return HttpResponse(' { "recipes" :  [] } ')

	rec_ings = '{ "recipes" : ' + form_json(recipes) + '}'

	return HttpResponse(rec_ings)


def search(request):
	term = request.GET.get('term')
	recipes = Recipe.objects.filter(name__icontains=term)

	if recipes.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	json = '{ "recipes" : ' + form_json(recipes) + '}'


	if json[15] == ':':
		json = json[:15] + json[(16):]

	return HttpResponse(json)

def find_friends(request):
	user = request.GET.get('username')
	friendships = Friendship.objects.filter(Q(from_user__username=user) | \
													Q(to_user__username=user)).select_related()

	if friendships.count() == 0:
		return HttpResponse(' { "users" :  [] } ') 

	friend_ids = []
	for f in friendships:
		if f.to_user.username == user:
			friend_ids.append(f.from_user)
		else:
			friend_ids.append(f.to_user)

	friends = User.objects.filter(username__in=friend_ids)

	json = simplejson.dumps( { 'users' : [{'username': f.username, 'email': f.email, 'first_name': f.first_name, 'last_name': f.last_name} for f in friends] } )

	return HttpResponse(json)


def find_recipes_full_user(request):
	user = request.GET.get('username')
	
	ingredients = OwnedPantry.objects.filter(owner__username=user)

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "select * from app_recipe where id in (select rp.owner_id from app_recipepantry as rp left join (select %d as id" % ingredients[0].item_id

	for i in ingredients[1:]:
		temp = " union all select %d" % i.item_id
		str = str + temp

	temp = ') search on rp.item_id = search.id group by rp.owner_id having count(case when search.id is null then 1 end) = 0);'
	
	str = str + temp

	recipes = Recipe.objects.raw(str)

	if len(list(recipes)) == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	rec_ings = '{ "recipes" : ' + form_json(recipes) + '}'

	return HttpResponse(rec_ings)


def find_recipes_partial_user(request):
	user = request.GET.get('username')
	
	ingredients = OwnedPantry.objects.filter(owner__username=user)

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "SELECT r.* FROM app_recipe r JOIN app_recipepantry rp ON rp.owner_id = r.id AND rp.item_id IN (%d" % ingredients[0].item_id

	for i in ingredients[1:]:
		temp = ",%d" % i.item_id
		str = str + temp
	
	str = str + ') GROUP BY r.name;'

	recipes = Recipe.objects.raw(str)

        if len(list(recipes)) == 0:
                return HttpResponse(' { "recipes" :  [] } ')

	rec_ings = '{ "recipes" : ' + form_json(recipes) + '}'
		
	return HttpResponse(rec_ings)


def friend_request(request):
	user = request.GET.get('from')
	first = request.GET.get('first')
	last = request.GET.get('last')

	try:
		to_user = User.objects.get(first_name__iexact=first, last_name__iexact=last) 
	except User.DoesNotExist:
		return HttpResponse(status=400)
	try:
		from_user = User.objects.get(username=user)
	except User.DoesNotExist:
		return HttpResponse(status=400)

	email_string = "You have a new friend request from %s %s on Thyme. To accept this friend request click the following link: " % (from_user.first_name, from_user.last_name)

	link = "http://ec2-54-200-100-22.us-west-2.compute.amazonaws.com/app/addfriendship?from=%s&to=%s" % (from_user.username, to_user.username)

	email_string = email_string + link

	send_mail('New Thyme friend request', email_string, 'thyme.friendship.adder@gmail.com', [to_user.email], fail_silently=False)

	return HttpResponse(status=200)


def add_friendship(request):
	to_username = request.GET.get('to')
	from_username = request.GET.get('from')

	try:
		to_user = User.objects.get(username=to_username)
	except User.DoesNotExist:
		return HttpResponse(status=400)

	try:
		from_user = User.objects.get(username=from_username)
	except User.DoesNotExist:
		return HttpResponse(status=400)

	friendship = Friendship.objects.filter(to_user=to_user,from_user=from_user)
	if friendship.count() > 0:
		return HttpResponse(status = 400) 
	friendship = Friendship.objects.filter(to_user=from_user,from_user=to_user)
	if friendship.count() > 0:
		return HttpResponse(status = 400) 


	friendship = Friendship(to_user=to_user,from_user=from_user)
	friendship.save()

	return HttpResponse(status=200)


def find_recipes_partial_multiple_user(request):
	user1 = request.GET.get('user1')
	user2 = request.GET.get('user2')
	

	ingredients = OwnedPantry.objects.filter(Q(owner__username=user1) | \
													Q(owner__username=user2))

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "SELECT r.* FROM app_recipe r JOIN app_recipepantry rp ON rp.owner_id = r.id AND rp.item_id IN (%d" % ingredients[0].item_id

	for i in ingredients[1:]:
		temp = ",%d" % i.item_id
		str = str + temp
	
	str = str + ') GROUP BY r.name;'

	recipes = Recipe.objects.raw(str)
	
	if len(list(recipes)) == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	rec_ings = '{ "recipes" : ' + form_json(recipes) + '}'
		
	return HttpResponse(rec_ings)


def find_recipes_full_multiple_user(request):
	user1 = request.GET.get('user1')
	user2 = request.GET.get('user2')
	

	ingredients = OwnedPantry.objects.filter(Q(owner__username=user1) | \
													Q(owner__username=user2))

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "select * from app_recipe where id in (select rp.owner_id from app_recipepantry as rp left join (select %d as id" % ingredients[0].item_id

	for i in ingredients[1:]:
		temp = " union all select %d" % i.item_id
		str = str + temp

	temp = ') search on rp.item_id = search.id group by rp.owner_id having count(case when search.id is null then 1 end) = 0);'
	
	str = str + temp

	recipes = Recipe.objects.raw(str)

	if len(list(recipes)) == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	rec_ings = '{ "recipes" : ' + form_json(recipes) + '}'

	return HttpResponse(rec_ings)

def create_user(request):
	username = request.GET.get('username')
	fname= request.GET.get('first')
	lname= request.GET.get('last')
	password= request.GET.get('password')
	email= request.GET.get('email')

	try:
		user = User.objects.create_user(username, email=email, password=password, first_name = fname, last_name = lname)
	except IntegrityError:
		#user exists already
		return HttpResponse(status=400)

	return HttpResponse(status=200)

def authenticate_user(request):
	username = request.GET.get('username')
	password= request.GET.get('password')

	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			print("User is valid, active and authenticated")
			return HttpResponse(status=200)
		else:
			return HttpResponse(status=400)
	else:
		#bad password
		return HttpResponse(status=400)


def add_recipe(request):
	name = request.GET.get('name')
	directions = request.GET.get('directions')
	rating = int(request.GET.get('rating'))
	reviews = request.GET.get('reviews')
	category = request.GET.get('category')
	time = int(request.GET.get('time'))
	uses_stove = int(request.GET.get('uses_stove'))
	uses_microwave = int(request.GET.get('uses_microwave'))
	uses_oven = int(request.GET.get('uses_oven'))
	is_kosher = int(request.GET.get('is_kosher'))
	is_vegetarian = int(request.GET.get('is_vegetarian'))
	is_nutfree = int(request.GET.get('is_nutfree'))
	is_glutenfree = int(request.GET.get('is_glutenfree'))
	is_lactosefree = int(request.GET.get('is_lactosefree'))
	
	entry = Recipe.objects.filter(name=name)
	if entry.count() > 0:
		#already exists
		return HttpResponse(status=400)

	to_add = Recipe(name=name, directions=directions, rating=rating, reviews=reviews, category=category, time=time, uses_stove=uses_stove, uses_microwave=uses_microwave, uses_oven=uses_oven, is_kosher=is_kosher, is_vegetarian=is_vegetarian, is_nutfree=is_nutfree, is_glutenfree=is_glutenfree, is_lactosefree=is_lactosefree)

	to_add.save()
	
	return HttpResponse(status=200)


def recipe_full_list(request):
	recipe_list = Recipe.objects.order_by('-name')

	json = simplejson.dumps( {"recipes" : [{'name' : i.name, 'directions' : i.directions, 'rating' : i.rating, 'reviews' : i.reviews, 'category' : i.category, 'time' : i.time, 'uses_stove' : i.uses_stove, 'uses_microwave' : i.uses_microwave, 'uses_oven' : i.uses_oven, 'is_kosher' : i.is_kosher, 'is_vegetarian' : i.is_vegetarian, 'is_nutfree' : i.is_nutfree, 'is_glutenfree' : i.is_glutenfree, 'is_lactosefree' : i.is_lactosefree} for i in recipe_list]} )

	return HttpResponse(json)

def add_ingredient_to_recipe(request):
	ing_name = request.GET.get('ingredient')
	rec_name = request.GET.get('recipe')
	amount = int(request.GET.get('amount'))

	try:
	   recipe = Recipe.objects.get(name=rec_name)
	except Recipe.DoesNotExist:
		return HttpResponse(status=400)

	try:
	   ingredient = Ingredient.objects.get(name=ing_name)
	except Ingredient.DoesNotExist:
		return HttpResponse(status=400)

	entry = RecipePantry.objects.filter(item=ingredient, owner=recipe, amount=amount)
	if entry.count() > 0:
		return HttpResponse(status=400)

	to_add = RecipePantry(item=ingredient, owner=recipe, amount=amount)
	to_add.save()
	
	return HttpResponse(status=200)

def user_empty_list(request):
	uname = request.GET.get('username')

	pantry = OwnedPantry.objects.filter(owner__username=uname, is_empty=0).select_related()

	json = simplejson.dumps( {"ingredients" :[{'item': p.item.name, 'unit': p.item.unit, 'amount': p.amount, 'is_empty': p.is_empty} for p in pantry]} )

	return HttpResponse(json)

def recipe_ingredient_list(request):
	name = request.GET.get('name')

	pantry = RecipePantry.objects.filter(owner__name=name).select_related()

	json = simplejson.dumps( {"ingredients" :[{'item': p.item.name, 'unit': p.item.unit, 'amount': p.amount} for p in pantry]} )

	return HttpResponse(json)

def multiple_user(user1, user2):

	ingredients = OwnedPantry.objects.filter(Q(owner__username=user1) | \
													Q(owner__username=user2))

	if ingredients.count() == 0:
		return HttpResponse(' { "recipes" :  [] } ') 

	str = "select * from app_recipe where id in (select rp.owner_id from app_recipepantry as rp left join (select %d as id" % ingredients[0].item_id

	for i in ingredients[1:]:
		temp = " union all select %d" % i.item_id
		str = str + temp

	temp = ') search on rp.item_id = search.id group by rp.owner_id having count(case when search.id is null then 1 end) = 0);'
	
	str = str + temp

	recipes = Recipe.objects.raw(str)

	return list(recipes)

def find_friend(request):
	recipe = request.GET.get('recipe') 
	user = request.GET.get('user') 
	friend_list = []

	friendships = Friendship.objects.filter(Q(from_user__username=user) | \
													Q(to_user__username=user)).select_related()

	if friendships.count() == 0:
		return HttpResponse(' { "users" :  [] } ') 

	friend_ids = []
	for f in friendships:
		if f.to_user.username == user:
			friend_ids.append(f.from_user)
		else:
			friend_ids.append(f.to_user)

	friends = User.objects.filter(username__in=friend_ids)

	for friend in friends:
		l = multiple_user(friend.username,user)

		for r in l:
			if r.name == recipe:
				friend_list.append(friend)

	if len(friend_list) == 0:
		return HttpResponse(' { "users" :  [] } ')

	json = simplejson.dumps( {"users" :[{'user': u.username, 'first': u.first_name, 'last': u.last_name, 'email': u.email} for u in friend_list]} )
	return HttpResponse(json)


def random(request):
	recipe_list = Recipe.objects.order_by('?')[0]

	json = simplejson.dumps( {"recipes" : [{'name' : i.name, 'directions' : i.directions, 'rating' : i.rating, 'reviews' : i.reviews, 'category' : i.category, 'time' : i.time, 'uses_stove' : i.uses_stove, 'uses_microwave' : i.uses_microwave, 'uses_oven' : i.uses_oven, 'is_kosher' : i.is_kosher, 'is_vegetarian' : i.is_vegetarian, 'is_nutfree' : i.is_nutfree, 'is_glutenfree' : i.is_glutenfree, 'is_lactosefree' : i.is_lactosefree} for i in [recipe_list]]} )

	return HttpResponse(json)

















