package utils;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseManager extends SQLiteOpenHelper {

	private SQLiteDatabase db;
	
	private static String name = "Thyme";
	private static int version = 1;
		
	public DatabaseManager(Context context){
		super(context, name, null, version);
	}
	
	public long addToItemAmount(String ingredient, int amount, String dateUsed, String dateAdded){
		ContentValues values = new ContentValues();
		this.db = getWritableDatabase();
		long status = -1;
		
		values.put("ingredient", ingredient);
		values.put("amount", amount);
		values.put("dateUsed", dateUsed);
		values.put("dateAdded", dateAdded);
		
		// ask the database object to insert the new data 
		try
		{
			status = db.insert("itemAmount", null, values);
			return status;
		}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString()); // prints the error message to the log
			e.printStackTrace(); // prints the stack trace to the log
		}
		return -1;
	}
	
	public long deleteFromItemAmount(String column, String value){
		try
		{
			this.db = getWritableDatabase();
		    long status = db.delete("itemAmount", column + "= '" + value + "'", null);
		    return status;
	    }
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}		
		return -1;
	}
	
	public void updateItemAmount(String ingredient, int amount, String dateUsed, String dateAdded)
	{		
		ContentValues values = new ContentValues();
		this.db = getWritableDatabase();
		
		values.put("ingredient", ingredient);
		values.put("amount", amount);
		values.put("dateUsed", dateUsed);
		values.put("dateAdded", dateAdded);
	 
		// ask the database object to update the database row of given rowID
		try {db.update("itemAmount", values, "ingredient = '" + ingredient+"'", null);}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}
	
	public ArrayList<ArrayList<Object>>getItemAmountRow(String column, String value){
		ArrayList<ArrayList<Object>> dataArrays =
				new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		this.db = getReadableDatabase();
	 
		try
		{
			if(column == null){
				cursor = db.query("itemAmount",null,null,null,null,null,null);
			}
			else{		
				cursor = db.query
				(
						"itemAmount",
						new String[] {"ingredients","amount","dateUsed","dateAdded"},
						column + "=" + value,
						null, null, null, null, null
				);
			}

			cursor.moveToFirst();
	 	 
			if (!cursor.isAfterLast())
			{
				do
				{
					ArrayList<Object> dataList = new ArrayList<Object>();
	 
					dataList.add(cursor.getString(0));
					dataList.add(cursor.getInt(1));
					
					String dU = cursor.getString(2);
					if(dU == null){
						dataList.add(null);
					}
					else{
						Date dateUsed = new Date(dU.substring(0,4),dU.substring(5, 7),dU.substring(8,10));
						dataList.add(dateUsed);					
					}
					String dA = cursor.getString(3);
					Date dateAdded = new Date(dA.substring(0,4),dA.substring(5, 7),dA.substring(8,10));

					dataList.add(dateAdded);
	 
					dataArrays.add(dataList);
				}
				// move the cursor's pointer up one position.
				while (cursor.moveToNext());
			}
		}
		catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	 
		return dataArrays;
		
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		//db.execSQL("create table pantry(item TEXT)");
		//db.execSQL("create table ingredients(name TEXT,unitOfMeasurement TEXT)");
		db.execSQL("CREATE TABLE itemAmount(ingredient TEXT PRIMARY KEY,amount INTEGER, dateUsed TEXT, dateAdded TEXT);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
