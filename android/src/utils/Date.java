package utils;

public class Date implements Comparable<Date>{

	int year;
	int month;
	int day;
	
	public Date(int y, int m, int d){
		this.year = y;
		this.month = m;
		this.day = d;
	}
	
	public Date(String y, String m, String d){
		this.year = Integer.parseInt(y);
		this.month = Integer.parseInt(m);
		this.day = Integer.parseInt(d);
	}
	
	@Override
	public String toString(){
		return year + "-" + month + "-" + day;
		
	}

	@Override
	public int compareTo(Date second) {
		if(year > second.year)
			return 1;
		else if(second.year > year)
			return -1;
		else{
			if(month > second.month)
				return 1;
			else if(second.month > month)
				return -1;
			else{
				if(day > second.day)
					return 1;
				else if(second.day > day)
					return -1;
				else
					return 0;
			}
		}
	}

}
