package view;

import com.kr.thyme.R;
import com.kr.thyme.R.layout;
import com.kr.thyme.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class Recipes extends Activity {
	Button favorite;
	Button find;
	Button top;
	Button random;
	Button created;
	Button recent;
	Button createNew;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recipes);
		
        favorite = (Button)findViewById(R.id.favoritesButton);
        favorite.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
        find = (Button)findViewById(R.id.findButton);
        find.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
        top = (Button)findViewById(R.id.topButton);
        top.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
        random = (Button)findViewById(R.id.randomButton);
        random.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
        created = (Button)findViewById(R.id.createdButton);
        created.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
        recent = (Button)findViewById(R.id.recentButton);
        recent.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
        createNew = (Button)findViewById(R.id.createButton);
        createNew.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.recipes, menu);
		return true;
	}

}
