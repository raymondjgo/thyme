package view;

import com.kr.thyme.R;
import com.kr.thyme.R.layout;
import com.kr.thyme.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class TabView extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_view);
		TabHost host = getTabHost();		
		
		Intent pantryIntent = new Intent(this,Pantry.class);
		TabSpec pantrySpec = host.newTabSpec("Pantry").setIndicator("Pantry").setContent(pantryIntent);
		host.addTab(pantrySpec);
		
		Intent recipeIntent = new Intent(this,Recipes.class);
		TabSpec recipeSpec = host.newTabSpec("Recipe").setIndicator("Recipe").setContent(recipeIntent);
		host.addTab(recipeSpec);
		
		Intent socialIntent = new Intent(this,Social.class);
		TabSpec socialSpec = host.newTabSpec("Social").setIndicator("Social").setContent(socialIntent);
		host.addTab(socialSpec);
		
		Intent shoppingIntent = new Intent(this,Shopping.class);
		TabSpec shoppingSpec = host.newTabSpec("Shopping").setIndicator("Shopping").setContent(shoppingIntent);
		host.addTab(shoppingSpec);
		
		Intent in = getIntent();
		Bundle bund = in.getExtras();
		host.setCurrentTab(bund.getInt("location"));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tab_view, menu);
		return true;
	}

}
