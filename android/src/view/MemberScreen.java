package view;

import com.kr.thyme.R;
import com.kr.thyme.R.layout;
import com.kr.thyme.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MemberScreen extends Activity {
	Button pantryButton;
	Button recipeButton;
	Button shoppingButton;
	Button socialButton;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_screen);
        pantryButton = (Button)findViewById(R.id.pantryButton);
        pantryButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent(MemberScreen.this,TabView.class);
				Bundle bund = new Bundle();
				bund.putInt("location",0);
				in.putExtras(bund);
				startActivity(in);				
			}
		});
        
        recipeButton = (Button)findViewById(R.id.recipeButton);
        recipeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent(MemberScreen.this,TabView.class);				
				Bundle bund = new Bundle();
				bund.putInt("location",1);
				in.putExtras(bund);
				startActivity(in);	
			}
		});
        
        socialButton = (Button)findViewById(R.id.socialButton);
        socialButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent(MemberScreen.this,TabView.class);				
				Bundle bund = new Bundle();
				bund.putInt("location",2);
				in.putExtras(bund);
				startActivity(in);				
			}
		});
        
        shoppingButton = (Button)findViewById(R.id.shoppingButton);
        shoppingButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent(MemberScreen.this,TabView.class);				
				Bundle bund = new Bundle();
				bund.putInt("location",3);
				in.putExtras(bund);
				startActivity(in);				
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.member_screen, menu);
        return true;
    }
    
    
}
