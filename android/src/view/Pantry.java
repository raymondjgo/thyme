package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.kr.thyme.R;
import com.kr.thyme.R.layout;
import com.kr.thyme.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import utils.DatabaseManager;
import utils.Date;

public class Pantry extends Activity implements OnItemSelectedListener {
    /** Called when the activity is first created. */
	String userSelection;
	String listSelection = "";
	Button add;
	Button delete;
	ListView lv;
	ArrayAdapter<String> la;
	String[] items={"Sort By", "Name","Date Added: Earliest First","Date Added: Latest First","Last Used"};
	ArrayList<String> listItems = new ArrayList<String>();
	ArrayList<ArrayList<Object>> results = new ArrayList<ArrayList<Object>>();
	Spinner spinner;
	DatabaseManager db = new DatabaseManager(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pantry);
        spinner=(Spinner)findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter aa=new ArrayAdapter(this, android.R.layout.simple_spinner_item,items);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);
        
        results = db.getItemAmountRow(null, null);
        for(ArrayList<Object> row : results){
        	listItems.add((String) row.get(0));
        }
        
        lv = (ListView) findViewById(R.id.list);
        la=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listItems);
        lv.setAdapter(la);
        
        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
              listSelection =(String) (lv.getItemAtPosition(myItemInt));
            }                 
      });
        
        add = (Button)findViewById(R.id.addButton);
        add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				long status = db.addToItemAmount("juice", 3, "2014-10-10", "2014-10-27");
				if(status != -1){
					repopulateList();
					la.notifyDataSetChanged();
				}
			}
		});
        delete = (Button)findViewById(R.id.deleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				if(listSelection != null && !listSelection.equals("")){
					long status = db.deleteFromItemAmount("ingredient", listSelection);
					repopulateList();
					la.notifyDataSetChanged();

				}
			}
		});
    }
    
    public void repopulateList(){
        results = db.getItemAmountRow(null, null);
        la.clear();
        for(ArrayList<Object> row : results){
        	la.add((String) row.get(0));
        }
    }
    
    public void sortByName(){
    	Collections.sort(listItems);
		la.notifyDataSetChanged();
    }
    
    public void sortByAdded(int dir){
    	if(dir == 0)
	    	Collections.sort(results,new Comparator<ArrayList<Object>>(){
	    		public int compare(ArrayList<Object> a1, ArrayList<Object> a2){
	    			return ((Date) a1.get(3)).compareTo((Date)a2.get(3));
	    		}
	    	});
    	else
        	Collections.sort(results,new Comparator<ArrayList<Object>>(){
        		public int compare(ArrayList<Object> a1, ArrayList<Object> a2){
        			return ((Date) a2.get(3)).compareTo((Date)a1.get(3));
        		}
        	});
    	
    	la.clear();
    	
    	for(ArrayList<Object> o : results){
    		la.add((String) o.get(0));
    	}
		la.notifyDataSetChanged();
    }
    
    public void sortByUsed(){
    	
    	Collections.sort(results,new Comparator<ArrayList<Object>>(){
    		public int compare(ArrayList<Object> a1, ArrayList<Object> a2){
    			if(a1.get(2) == null && a2.get(2) == null)
    				return 0;
    			else if(a1.get(2) == null)
    				return 1;
    			else if(a2.get(2) == null)
    				return -1;
    			else
    				return ((Date) a2.get(2)).compareTo((Date)a1.get(2));
    		}
    	});

    	
    	la.clear();
    	
    	for(ArrayList<Object> o : results){
    		la.add((String) o.get(0));
    	}
		la.notifyDataSetChanged();
    }
    
	@Override
	public void onItemSelected(AdapterView arg0, View arg1, int pos,
			long arg3) {
		userSelection = items[pos];
		if(userSelection.equals("Name"))
			sortByName();
		else if(userSelection.equals("Date Added: Earliest First"))
			sortByAdded(0);
		else if(userSelection.equals("Date Added: Latest First"))
			sortByAdded(1);
		else if(userSelection.equals("Last Used"))
			sortByUsed();
	}
	@Override
	public void onNothingSelected(AdapterView arg0) {
		// TODO Auto-generated method stub
		userSelection = "";
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pantry, menu);
		return true;
	}
}
