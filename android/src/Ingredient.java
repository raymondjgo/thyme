public class Ingredient {
   private String name;

   private String unitOfMeasurement;

   public Ingredient(String n, String u) {
      this.name = n;
      this.unitOfMeasurement = u;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String n) {
      this.name = n;
   }

   public String getUnitOfMeasurement() {
      return this.unitOfMeasurement;
   }

   public void setUnitOfMeasurement(String u) {
      this.unitOfMeasurement = u;
   }
}
